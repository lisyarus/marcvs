###################################

# MARCVS SERIALIZATION PROTOCOL
# by solarcold

###################################

import hashlib

from .element import *
from .bytecoders import *
from .utility import *
from .exceptions import *

###################################

class Package:

	def __init__(self):

		self.initialize()

	def initialize(self):

		self._elements = dict()
		self._preset_tp_codes = dict()

	@property
	def list_element_names(self):
		
		return [element.name for name, element in self._elements.items()]

	def add_element(self, name, val, tp_code = None):

		new_element = Element(name, val, tp_code = tp_code)

		self._elements[new_element.name] = new_element

	def has_element(self, name):

		if str(name) in self._elements:
			return True
		return False

	def get_element(self, name):

		if self.has_element(str(name)):
			return self._elements[str(name)]
		raise WrongElementName

	def get_value(self, name):

		return self.get_element(name).value

	def set_value(self, name, val, tp_code = None):

		if self.has_element(name):
			
			element = self.get_element(name)
			element.tp_code = tp_code
			element.value = val
			
		else:

			if name in self._preset_tp_codes:
				tp_code = self._preset_tp_codes.pop(name)

			self.add_element(name, val, tp_code = tp_code)

	def set_tp_code(self, name, tp_code):

		if self.has_element(name):
			
			element = self.get_element(name)
			element.tp_code = tp_code
			
		else:
			
			self._preset_tp_codes[name] = tp_code

	def pack(self):

		packed = bytes()
		
		count = succinct_len(len(self._elements))
		
		for name in self._elements:
			packed += self.get_element(name).pack()
		
		hasher = hashlib.sha256()
		hasher.update(packed)
		byte_hash = hasher.digest()
		
		full_len = succinct_len(len(byte_hash)+len(count)+len(packed))
		
		return full_len + byte_hash + count + packed
	
	
	def unpack(self,byte_stream, ignore_hash = False):

		self.initialize()
		cursor = 0

		def read_bs(bs, shift):
			nonlocal cursor
			res = bs[cursor:cursor+shift]
			cursor += shift
			return res
		
		full_len_flag = bytes_to_unsigned_int(read_bs(byte_stream, 1))
		full_len = bytes_to_unsigned_int(read_bs(byte_stream, full_len_flag))
		
		byte_hash_check = read_bs(byte_stream, 32)
		
		count_flag = bytes_to_unsigned_int(read_bs(byte_stream, 1))
		count = bytes_to_unsigned_int(read_bs(byte_stream, count_flag))
		
		byte_stream = byte_stream[cursor:]
		
		hasher = hashlib.sha256()
		hasher.update(byte_stream)
		byte_hash = hasher.digest()		

		if (byte_hash == byte_hash_check) or (ignore_hash):
			
			for c in range(count,0,-1):

				new_element = Element(None,None)
				byte_stream = new_element.unpack(byte_stream)
				self._elements[new_element.name] = new_element
		
		else:
			
			raise HashMismatch

		
	def pretty_print(self):

		print ('==marcvs=pck='+('='*50))
		for _, element in self._elements.items():
			val = element.value
			if type(val)==bytes:
				val = brepr(val)
			print(':: {} :: {} :: {}'.format(shorten(element.name,20),
							shorten(val,40),
							shorten(element.byte_coder_name,30)))
		print ('='*63)



class PackageObject(Package):

	def __init__(self):

		super().__init__()

	def __getattr__(self, attr):

		if attr in ('_elements','_preset_tp_codes'):
			return self.__dict__[attr]
		return self.get_value(attr)

	def __setattr__(self, attr, val):

		if attr in ('_elements','_preset_tp_codes') and (val == dict()):
			self.__dict__[attr] = dict()
		elif (attr!='_elements'):
			self.set_value(attr, val)

	def setup_field(self, name, tp_code):

		self.set_tp_code(name, tp_code)
