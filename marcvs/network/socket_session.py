from .sockets import *
from .netpackage import *


class OneWaySocketSession:

	def __init__(self, host, port):

		self.session_id = None

		self.host = host
		self.port = port

		self.mainpipe = None

		self.incoming_queue = []

	def is_connected(self):

		return not (self.mainpipe == None)

	def drop(self):

		self.host = None
		self.port = None

		self.mainpipe = None

		self.session_id = None

	def send_netpackage(self, np):

		sck = self.mainpipe
		sck.send_netpackage(np)

		np = None
		
		while not np:
			np = sck.read_from_socket()

		return np

	def connect(self, host, port):

		self.host = host
		self.port = int(port)
		self.initialize_connection()

	def initialize_connection(self):

		self.mainpipe = GenericSocket(self.host, self.port)

		self.mainpipe.connect()

		np = CreateSessionPackage()
		self.session_id = np.session_id

		result_np = self.send_netpackage(np)



class GenericSocketSession:

	def __init__(self, host, port):

		self.session_id = None

		self.host = host
		self.port = port

		self.mainpipe = None
		self.listener = None

		self.incoming_queue = []

	def is_connected(self):

		return (self.mainpipe and self.listener)

	def drop(self):

		self.host = None
		self.port = None

		self.mainpipe = None
		self.listener = None

		self.session_id = None

		self.incoming_queue = []

	def send_netpackage(self, np, listener = False):

		if listener: sck = self.listener
		else: sck = self.mainpipe

		sck.send_netpackage(np)

		np = None

		while not np:
			np = sck.read_from_socket()

		return np

	def connect(self, host, port):

		self.host = host
		self.port = int(port)
		self.initialize_connection()

	def initialize_connection(self):

		self.mainpipe = GenericSocket(self.host, self.port)

		self.mainpipe.connect()

		np = CreateSessionPackage()
		self.session_id = np.session_id

		result_np = self.send_netpackage(np)

		if result_np.status == 'success':
			
			self.listener = ThreadedListener(self.host, self.port, self)

			self.listener.connect()

			np = AssignListenerPackage(self.session_id)

			result_np = self.send_netpackage(np, listener = True)

			if result_np.status == 'success':

				self.run_listener()

	def handle_queue(self):

		while len(self.incoming_queue)>0:
			self.handle_netpackage(self.incoming_queue.pop(0))

	def handle_netpackage(self, np):

		print ('\nLISTENER CAUGHT:\n')
		np.pretty_print()

	def run_listener(self):

		self.listener.thread.start()
