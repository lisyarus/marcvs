from .server_session import *
from .netpackage import *

class GenericServerRouter:

	def __init__(self, server):

		self.server = server

	def process_request(self, raw_request, transport = None):

		request = self.custom_prepare_before_route(raw_request, transport = transport)
		response = self.route_request(request, transport = transport)

		return response

	def custom_prepare_before_route(self, raw_request, transport = None):

		return raw_request

	def route_request(self, request, transport = None):

		return request



class MarcvsServerRouter(GenericServerRouter):

	def __init__(self, server):

		super().__init__(server)
		self.request_handlers = dict()
		
		self.register_request_handler(CREATE_SESSION_REQUEST,
								self.handle_create_session)
		
		self.register_request_handler(ASSIGN_LISTENER_REQUEST,
								self.handle_assign_listener)
		
		self.register_request_handler(ECHO_REQUEST,
								self.handle_echo)

	def register_request_handler(self, request_code, handler_func):
		
		self.request_handlers[request_code] = handler_func

	def custom_prepare_before_route(self, raw_request, transport = None):

		request_netpackage = NetPackage(None, None)
		request_netpackage.unpack(raw_request)

		return request_netpackage

	def route_request(self, request, transport = None):
		print('REQ',request)
		if request.package_code in self.request_handlers:
			return self.request_handlers[request.package_code](request, transport = transport)
		
		else:
			return self.custom_route_request(request, transport = transport)
		

	def custom_route_request(self, request, transport = None):

		return None
	
	###
	# REQUEST HANDLERS
	###
	
	def handle_create_session(self, request, transport = None):
		
		session_id = request.session_id
		
		if session_id:
			session = self.server.get_session(session_id)
			
			if (not session) and (session_id):

				new_session = GenericServerSession(session_id)
				result = new_session.set_mainpipe(transport)
					
				if result:
					
					self.server.add_session(new_session)
					return SuccessNetPackage(session_id)

		return FailureNetPackage(session_id)
	
	def handle_assign_listener(self, request, transport = None):
		
		session_id = request.session_id
		
		if session_id:
			session = self.server.get_session(session_id)
			
			if session:
				result = session.set_listener(transport)

				if result:
					
					session.pretty_print()
					return SuccessNetPackage(session_id)

		return FailureNetPackage(session_id)
	
	def handle_echo(self, request, transport = None):
		
		session_id = request.session_id
		
		if session_id:
			session = self.server.get_session(session_id)
				
			if session:
				return request

		return FailureNetPackage(session_id)
