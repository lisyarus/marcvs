#####################################

# MARCVS SERIALIZATION PROTOCOL
# by solarcold

###################################

import uuid

from ..package import *

###################################

class NetPackage(PackageObject):

	def __init__(self, session_id, package_code):

		super().__init__()

		"""Net packages are tied to connection id (referring
		to session containing of bundled mainpipe and listener
		sockets"""

		self.set_tp_code('session_id', 10)
		self.set_tp_code('package_code', 12)

		if session_id == None:
			self.session_id = uuid.uuid4().bytes
		else:
			self.session_id = session_id

		"""Net packages have obligatory package_code. Some of them
		are reserved"""

		if package_code == None:

			self.package_code = 0
		else:
			self.package_code = package_code


###################################


CREATE_SESSION_REQUEST = 1
ASSIGN_LISTENER_REQUEST = 2
GENERIC_STATUS_RESPONSE = 3
ECHO_REQUEST = 4

class CreateSessionPackage(NetPackage):

	def __init__(self):

		super().__init__(None, CREATE_SESSION_REQUEST)


class AssignListenerPackage(NetPackage):

	def __init__(self, session_id):

		super().__init__(session_id, ASSIGN_LISTENER_REQUEST)

class SuccessNetPackage(NetPackage):

	def __init__(self, session_id):

		super().__init__(session_id, GENERIC_STATUS_RESPONSE)
		self.status = 'success'

class FailureNetPackage(NetPackage):

	def __init__(self, session_id):

		super().__init__(session_id, GENERIC_STATUS_RESPONSE)
		self.status = 'failure'
