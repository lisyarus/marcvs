import socket
import threading

from .netpackage import *
from ..utility import *

class GenericSocket:

	def __init__(self, host, port):

		self.underlying_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.underlying_socket.settimeout(4)
		
		self.host = host
		self.port = port

		self.incoming_bytes_reader = bytes()

		#print ('SOCKET INITIALIZED:',self,self.underlying_socket)

	def connect(self):
		self.underlying_socket.connect((self.host, self.port))

	def send_netpackage(self, np):

		self.underlying_socket.sendall(np.pack())

	def set_timeout(self, seconds):
		
		self.underlying_socket.settimeout(seconds)

	def read_from_socket(self):

		np = None

		expected_length = -1

		chunk = self.underlying_socket.recv(256)

		if chunk:

			self.incoming_bytes_reader += chunk
			
			if expected_length<0:
				
				full_len_flag = bytes_to_unsigned_int(self.incoming_bytes_reader[0:1])
				expected_length = bytes_to_unsigned_int(self.incoming_bytes_reader[1:1+full_len_flag])
				
			if len(self.incoming_bytes_reader)>=expected_length+1+full_len_flag:

				incoming_bytes = self.incoming_bytes_reader[:expected_length+1+full_len_flag]
				
				self.incoming_bytes_reader = self.incoming_bytes_reader[expected_length+1+full_len_flag:]
				np = self.form_netpackage_from_bytes(incoming_bytes)

				expected_length = -1

		return np
	
	def form_netpackage_from_bytes(self, b):
		
		np = NetPackage(None, None)
		np.unpack(b)
		
		return np

class ThreadedListener(GenericSocket):

	def __init__(self, host, port, socket_session):

		self.socket_session = socket_session
		super().__init__(host, port)

		self.thread = threading.Thread(target=self.process_listener_queue, daemon=True)

	def process_listener_queue(self):

		while True:

			np = None
			while not np:
				np = self.read_from_socket()

			self.socket_session.incoming_queue.append(np)
			self.socket_session.handle_queue()
