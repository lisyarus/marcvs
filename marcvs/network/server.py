import asyncio
import time 

from .server_protocol import *

class GenericServer:

	def __init__(self, host, port, router = None):

		self.host = host
		self.port = port

		self.router = router

		self.sessions = dict()

	def set_router(self, router):

		self.router = router

	def binded_protocol_factory(self):

		return GenericServerProtocol(self.router)

	def add_session(self, session):

		self.sessions[session.session_id] = session
		print ('\nNew session established:')
		session.pretty_print()
		print ('\nCurrently on: {} sessions.'.format(len(self.sessions)))

	def get_session(self, session_id):

		if session_id in self.sessions:
			return self.sessions[session_id]

	def close_session_by_transport(self, transport):

		for session_id, s in self.sessions.items():

			if transport in (s.mainpipe, s.listener):
				s.close()
				self.sessions.pop(s.session_id)
				break

	async def run(self):

		loop = asyncio.get_event_loop()

		async_server = await loop.create_server(
			self.binded_protocol_factory,
			host = self.host,
			port = self.port
			)
		async with async_server:
			print ('====\nServer initialized. Serving.\n====\n')
			await async_server.serve_forever()
