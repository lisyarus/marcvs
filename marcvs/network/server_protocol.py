import asyncio

class GenericServerProtocol(asyncio.Protocol):

	def __init__(self, router):

		super().__init__()
		self.router = router
		self.transport = None
		print ('Server: Protocol initialized')

	def connection_made(self, transport):

		print ('Connection established. Client:\n', transport)
		self.transport = transport

	def data_received(self, data):

		print ('\n====\nData received by server')
		print ('Transport: {}\n====\n'.format(self.transport))

		response_netpackage = self.router.process_request(data, transport = self.transport)

		raw_response = response_netpackage.pack()

		print ('\n====\nData sent out by server')
		print ('Transport: {}\n====\n'.format(self.transport))

		self.transport.write(raw_response)

	def connection_lost(self, exc):

		self.router.server.close_session_by_transport(self.transport)

		print ('Connection lost. Transport: {}'.format(self.transport), exc)
