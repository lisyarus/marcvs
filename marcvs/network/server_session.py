class GenericServerSession:
	"""A session connected to the socket server, consisting
	of the mainpipe and the listener
	"""

	def __init__(self, session_id):

		self.session_id = session_id
		self.mainpipe = None
		self.listener = None

	def set_mainpipe(self, transport):

		if transport:
			self.mainpipe = transport
			self.mainpipe.session_id = self.session_id
			return True
		return False

	def set_listener(self, transport):

		if transport:
			self.listener = transport
			self.listener.session_id = self.session_id
			return True
		return False

	def send_to_mainpipe(self, netpackage, raw=False):

		if self.mainpipe:
			if not raw:
				netpackage = netpackage.pack()

			self.mainpipe.write(netpackage)
			
	def send_to_listener(self, netpackage, raw=False):

		if self.listener:
			if not raw:
				netpackage = netpackage.pack()

			self.listener.write(netpackage)

	def pretty_print(self):

		print ('=========session='+('='*46))
		print (':: session_id :: {}'.format(str(self.session_id)))
		print (':: mainpipe :: {}'.format(self.mainpipe))
		print (':: listener :: {}'.format(self.listener))
		print ('='*63)

	def close(self):

		try:
			self.mainpipe.close()
		except: pass

		try:
			self.listener.close()
		except: pass

		self.mainpipe = None
		self.listener = None
