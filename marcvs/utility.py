def brepr(b):
	"""Represent long bytes sequences as neat hex 
	shorthands enclosed in triangle brackets
	"""

	try:
		full = b.hex()

		if len(full)>12:
			return '<<'+full[0:6]+'..'+full[-6:]+'>>'
		else: return '<<'+full+'>>'
	
	except:
		return '<<??????..??????>>'

def shorten(s, i):
	"""Shortens string s to a string ending with
	two periods, with length i
	"""
	
	s = str(s)
	if len(s)<i-3:
		return s
	else:
		return s[:i]+'..'

def pretty_raw_netpackage(b):
	"""Shows entire netpackage as a table. Bytes,
	if possible, are represented as characters.
	"""

	print ('==RAW=marcvs=pck='+('='*46))
	for i in range(0,len(b),8):
		line = ''
		for j in range(0,min(8,len(b)-i)):
			bt = b[i+j]
			if (bt>32)and(bt<126): bt = chr(bt)
			else: bt = '/'+str(bt)
			line += '  '+bt.ljust(5)+'|'
		print (line)
		print ('-'*63)
	print ('='*63)
	
def signed_int_to_bytes(i, bytelen = None):

	if not bytelen:
		return i.to_bytes((i.bit_length() + 8) // 8, byteorder="big", signed = True)
	else: return i.to_bytes(bytelen, byteorder='big', signed = True)

def unsigned_int_to_bytes(i, bytelen = None):

	if not bytelen:
		return i.to_bytes((i.bit_length() + 7) // 8, byteorder="big")
	else: return i.to_bytes(bytelen, byteorder='big')

def bytes_to_signed_int(b):
	
	return int.from_bytes(b, byteorder='big', signed = True)

def bytes_to_unsigned_int(b):
	
	return int.from_bytes(b, byteorder='big')

def bytes_bits(b):
	
	masks = [
		int('10000000',2),
		int('01000000',2),
		int('00100000',2),
		int('00010000',2),
		int('00001000',2),
		int('00000100',2),
		int('00000010',2),
		int('00000001',2)
		]
	
	res = ''
	for bt in b:
		for m in masks:
			res += '1' if m&bt else '0'
		res += ' '
	return res

def succinct_len(i):
	"""Represents i as a sequence of bytes (h, l),
	where l is a minimal byte representation of int i,
	and h is one byte representing the length of l
	"""
	
	l = unsigned_int_to_bytes(i)
	h = unsigned_int_to_bytes(len(l), bytelen = 1)
		
	return h+l
