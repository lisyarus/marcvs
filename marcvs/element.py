###################################

# MARCVS SERIALIZATION PROTOCOL
# by solarcold

###################################

from .bytecoders import *

###################################

class Element:
	"""This class represents a singular, atomic element of
	a marcvs package.
	"""

	def __init__(self, name, value, tp_code = None):
		"""Initializes an element.
		
		If the tp_code argument is set,
		this element will be forced to use a particular bytecoder type.
		
		If it's not set, the bytecoder type will be assumed from the value
		type, if it exists in the bytecoders library."""

		self.initialize()
		self.tp_code = tp_code

		if name!=None: self.name = name
		if value!=None: self.value = value
		
	def initialize(self):
		
		self.__name = None
		self.__value = None
		
	@property
	def byte_coder(self):
		
		return BYTE_CODERS[self.tp_code]

	@property
	def byte_coder_name(self):
		
		if self.tp_code in BYTE_CODERS:
			return str(BYTE_CODERS[self.tp_code].__class__.__name__)
		else:
			return 'NO_BYTECODER'

	@property
	def name(self):
		
		return self.__name

	@property
	def value(self):
		
		return self.__value

	@name.setter
	def name(self, val):
		
		self.__name = val

	@value.setter
	def value(self, val):
		
		if (type(val) in BASIC_TYPES) and (self.tp_code == None):
			self.tp_code = BASIC_TYPES[type(val)]
			
		self.__value = val
		
	@property
	def packed_name(self):
		
		return BYTE_CODERS[STRING_TP_CODE].to_bytes(self.__name)
	
	@property
	def packed_value(self):
		
		return self.byte_coder.to_bytes(self.__value)
	
	def unpack_name(self, b):
		self.name = BYTE_CODERS[STRING_TP_CODE].from_bytes(b)
		
	def unpack_value(self, b):
		self.value = self.byte_coder.from_bytes(b)

	def pack(self):
		
		packed_name = self.packed_name
		packed_value = self.packed_value
		
		namelen = succinct_len(len(packed_name))
		tpcode = unsigned_int_to_bytes(self.tp_code, bytelen = 2)
		vallen = succinct_len(len(packed_value))

		return namelen + packed_name + tpcode + vallen + packed_value

	def unpack(self, byte_stream):

		cursor = 0

		def read_bs(bs, shift):
			nonlocal cursor
			res = bs[cursor:cursor+shift]
			cursor += shift
			return res

		namelen_flag = bytes_to_unsigned_int(read_bs(byte_stream, 1))
		namelen = bytes_to_unsigned_int(read_bs(byte_stream, namelen_flag))
		
		self.unpack_name(read_bs(byte_stream,namelen))

		self.tp_code = int.from_bytes(read_bs(byte_stream,2),byteorder='big')

		vallen_flag = bytes_to_unsigned_int(read_bs(byte_stream, 1))
		vallen = bytes_to_unsigned_int(read_bs(byte_stream, vallen_flag))
		
		self.unpack_value(read_bs(byte_stream,vallen))

		return byte_stream[cursor:]
