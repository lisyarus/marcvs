###################################

# MARCVS SERIALIZATION PROTOCOL
# by solarcold

###################################

class TypeMismatch(Exception):

    def __init__(self, expected_type, caught_type):
        super().__init__('Type mismatch during byte conversion. '+
			'Expected: {}, got {} instead.'.format(str(expected_type),
										  str(caught_type)))

class MemberTypeUnknown(Exception):

    def __init__(self):
        super().__init__('A member of the given value has an unknown type.')

class WrongElementName(Exception):

    def __init__(self):
        super().__init__('Wrong element name supplied')

class HashMismatch(Exception):

    def __init__(self):
        super().__init__('Hash mismatch')
