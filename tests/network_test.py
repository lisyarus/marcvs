from marcvs.network.server import GenericServer
from marcvs.network.server_router import MarcvsServerRouter
import asyncio

host = '127.0.0.1'
port = 7333

SRV = GenericServer(host, port)
SRV.set_router(MarcvsServerRouter(SRV))
asyncio.run(SRV.run())
