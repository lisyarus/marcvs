import marcvs
import random

from datetime import date
from decimal import Decimal

# TEST FUNCTIONS

def test_element(elem):
	
	try:
		packed = elem.pack()
		new = marcvs.Element(None, None)
		new.unpack(packed)
		assert new.name == elem.name
		assert new.value == elem.value
			
	except:
		print('Error=====')
		print(elem.value)
		print(new.value)
		
		print('Error=====')
		
def test_package(package):
	
	packed = package.pack()
	
	new = marcvs.Package()
	new.unpack(packed)
	
	for name in package.list_element_names:
		elem_init = package.get_element(name)
		elem_unp = new.get_element(name)
		
		assert elem_init.name == elem_unp.name
		assert elem_init.value == elem_unp.value
		
	
# RANDOM BASIC TYPES
		
def random_int():
	return random.randint(-10000000,10000000)
	
def random_float():
	return random.uniform(-10000000,10000000)

def random_bytes():
	return random.randbytes(random.randint(0,100))

def random_date():
	y = random.randint(1990,2400)
	m = random.randint(1,12)
	d = random.randint(1,28)
	return date(y,m,d)

def random_decimal():
	return Decimal(random_float())

def random_boolean():
	return random.randint(0,100)>50


def random_unicode():
	# This function courtesy of Jacob Wan from stackoverflow.
	length = random.randint(3,100)
	
	try:
		get_char = unichr
	except NameError:
		get_char = chr

	# Update this to include code point ranges to be sampled
	include_ranges = [
		( 0x0021, 0x0021 ),
		( 0x0023, 0x0026 ),
		( 0x0028, 0x007E ),
		( 0x00A1, 0x00AC ),
		( 0x00AE, 0x00FF ),
		( 0x0100, 0x017F ),
		( 0x0180, 0x024F ),
		( 0x2C60, 0x2C7F ),
		( 0x16A0, 0x16F0 ),
		( 0x0370, 0x0377 ),
		( 0x037A, 0x037E ),
		( 0x0384, 0x038A ),
		( 0x038C, 0x038C ),
	]

	alphabet = [
		get_char(code_point) for current_range in include_ranges
			for code_point in range(current_range[0], current_range[1] + 1)
	]
	return ''.join(random.choice(alphabet) for i in range(length))

# Random containers

def random_list(lvl):

	length = random.randint(0,20)
	
	res = []
	
	for i in range(0,length):
		if lvl>2:
			res.append(random_int())
		else:
			res.append(random_value(random_tp_code(),lvl+1))
	return res

def random_dict(lvl):

	length = random.randint(0,20)
	
	res = dict()
	
	for i in range(0,length):
		if lvl>2:
			res[random_unicode()] = random_int()
		else:
			res[random_unicode()] = random_value(random_tp_code(),lvl+1)
	return res

def random_set(lvl):
	return random_list(lvl)

def random_tuple(lvl):
	return random_list(lvl)

# Random element

def random_tp_code():
	i = random.randint(0, len(marcvs.bytecoders.BYTE_CODERS)-1)
	return list(marcvs.bytecoders.BYTE_CODERS.keys())[i]

def random_value(tp,lvl):
	#print(tp)
	if tp==marcvs.bytecoders.BYTES_TP_CODE:
		return random_bytes()
	elif tp==marcvs.bytecoders.STRING_TP_CODE:
		return random_unicode()
	elif tp==marcvs.bytecoders.INTEGER_TP_CODE:
		return random_int()
	elif tp==marcvs.bytecoders.FLOAT_TP_CODE:
		return random_float()
	elif tp==marcvs.bytecoders.NONE_TP_CODE:
		return None
	elif tp==marcvs.bytecoders.BOOLEAN_TP_CODE:
		return random_boolean()
	elif tp==marcvs.bytecoders.LIST_TP_CODE:
		return random_list(lvl)
	elif tp==marcvs.bytecoders.DICT_TP_CODE:
		return random_dict(lvl)
	elif tp==marcvs.bytecoders.DATE_TP_CODE:
		return random_date()
	elif tp==marcvs.bytecoders.DECIMAL_TP_CODE:
		return random_decimal()
	elif tp==marcvs.bytecoders.TUPLE_TP_CODE:
		return random_tuple(lvl)
	elif tp==marcvs.bytecoders.SET_TP_CODE:
		return random_set(lvl)

def random_element():
	
	elem = marcvs.Element('random',random_list(0))
		
	return elem

def random_package():
	
	length = random.randint(0,20)
	
	package = marcvs.Package()
	for i in range(0, length):
		e = random_element()
		package.add_element(e.name, e.value)

	return package

print('If anything except for this sentence comes up on screen, then something is wrong.')

for i in range(0,10000):
	test_package(random_package())

for i in range(0,10000):
	test_element(random_element())
