from marcvs.network.socket_session import *

host = '127.0.0.1'
port = 7333

S = OneWaySocketSession(host, port)
S.initialize_connection()
